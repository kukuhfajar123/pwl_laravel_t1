@extends('template.main')
@section('title', 'hobi_ku')
@section('container')
<div class="container mt-5">
    <h1>hobi ku</h1>
    <h3>Saya memiliki hobi berolahraga meliputi:</h3>
    @foreach($data as $hobi)
        <div class="mt-3 mx-5">
            <li><b>{{ $hobi[0] }}</b></li>
            <ol>{{ $hobi[1] }} </ol>
        </div>
    @endforeach
</div>
@endsection
