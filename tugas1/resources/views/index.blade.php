@extends('template.main')
@section('title', 'Profile_Ku')
@section('container')
<div class="container mt-5">
    <h1>Hello, Semua!</h1>
    <h3 class="m-5">Perkenalkan, Nama saya {{ $nama }}</h3>
    <div class="mt-5" style="margin-left:10%; margin-right: 10%; text-align:justify;">
        <h6 class="mb-3">Saya Berasal dari {{ $alamat }}</h6>
        <p>{{ $deskripsi }}</p>
    </div>
</div>
@endsection
