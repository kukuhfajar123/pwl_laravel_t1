@extends('template.main')
@section('title', 'pendidikan_ku')
@section('container')
<div class="container mt-5">
    <h1>Riwayat Pendidikan</h1>
    <div class="m-5">
        <h3>{{ $status_smp }}</h3>
        <ul class="ml-3">
            <li>SMP : {{ $smp }}</li>
        </ul>
        <h3>{{ $status_smk }}</h3>
        <ul class="ml-3">
            <li>SMK : {{ $smk }}</li>
            <ol>{{$jurusan_smk}}</ol>
        </ul>
        <h3>{{ $status_kuliah }}</h3>
        <ul class="ml-3">
            <li>Kuliah : {{ $kuliah }}</li>
            <ol>{{$fakultas}} {{$program_studi}}</ol>
        </ul>
    </div>
</div>
@endsection
