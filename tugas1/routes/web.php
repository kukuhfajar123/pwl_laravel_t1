<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $profile=[
        'nama' => 'Kukuh Fajar Ribawanto',
        'alamat' => 'Ngadiluwih, Kab. Kediri, Jawa Timur',
        'deskripsi' => 'Saya anak laki-laki yang sholeh dan pintar yang sedang menempuh pendidikan di Universitas Brawijaya Fakultas Ilmu Komputer program studi Sistem Informasi.',
    ];
    return view('index', $profile);
});

Route::get('/pendidikan', function () {
    $pendidikan=[
        'smp' => 'SMP Negeri 1 Ngadiluwih',
        'status_smp' => '2012 - 2015',
        'smk' => 'SMK Negeri 1 Kediri',
        'status_smk' => '2015 - 2018',
        'jurusan_smk' => 'Teknik Komputer & Jaringan',
        'kuliah' => 'Universitas Brawijaya',
        'fakultas' => 'Fakultas Ilmu Komputer',
        'program_studi' => 'Sistem Informasi',
        'status_kuliah' => '2018 - Sekarang',
    ];
    return view('pendidikan', $pendidikan);
});

Route::get('/hobi', function () {
    $hobiku=[
        ["Basket", "menyukai hobi ini sejak smp"],
        ["Bulu Tangkis", "menyukai hobi ini sejak kecil"],
        ["Sepak Bola", "menyukai hobi ini sejak kecil"],
        ["Futsal", "menyukai hobi ini sejak smk"],
        ["Berenang", "menyukai hobi ini sejak kecil"],
        ["Bersepeda", "menyukai hobi ini sejak kecil"],
    ];
    return view('hobi', ['data'=>$hobiku]);
});

